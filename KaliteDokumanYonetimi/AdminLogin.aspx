﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminLogin.aspx.cs" Inherits="KaliteDokumanYonetimi.AdminLogin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
      <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <h1><strong>Çankırı Devlet Hastanesi</strong></h1>
                            <h1><strong>SKS Döküman Yönetici Girişi</strong></h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                            		<h3><b>Giriş yapmak için kullanıcı adı ve şifrenizi giriniz;</b></h3>
                        		</div>
                        		<div class="form-top-right">
                        			<i class="fa fa-key"></i>
                        		</div>
                            </div>
                            <div class="form-bottom">
			                    <form role="form"method="post" class="login-form">
			                    	<div class="form-group">
			                    		<label class="sr-only" for="form-username">Kullanıcı Adı</label>
                                        <asp:TextBox name="form-username" placeholder="Kullanıcı Adı..." class="form-username form-control" ID="TextBox1" runat="server"></asp:TextBox>
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-password">Şifre</label>
                                        <asp:TextBox ID="TextBox2" runat="server" type="password" name="form-password" placeholder="Şifre..." class="form-password form-control"></asp:TextBox>
			                    
			                        </div>

                                    <asp:Button ID="Button1" runat="server" Text="Giriş Yap" CssClass="btn-success" OnClick="Button1_Click" />
                                    &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                                    <asp:Button ID="Button2" runat="server" Text="Şifremi Unuttum?" CssClass="btn-danger"   />
                                    
                                </form>

                                <asp:Label ID="Label3" runat="server" Text="Label" Visible="false"></asp:Label>
		                    </div>
                        </div>
                    </div>
               
                </div>
            </div>
 
            
        </div>


        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/scripts.js"></script>
        
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->





    </div>
    </form>
</body>
</html>
