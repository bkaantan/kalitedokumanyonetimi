﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KaliteDokumanYonetimi
{
    public partial class AdminLogin : System.Web.UI.Page
    {
        String islem = "";
        Veritabani n = new Veritabani();
        protected void Page_Load(object sender, EventArgs e)
        {
            islem = Request.QueryString["islem"];

            if (islem == "cik")

            {              
                Session.Remove("admin");
                Session.Remove("user");
                Response.Redirect("AdminLogin.aspx");
            }

            if (Session["admin"] != null)
            {
                Label3.Visible = false;
                Response.Redirect("Anasayfa.aspx");
            }
            else if(Session["user"] != null)
            {
                Label3.Visible = false;
                Response.Redirect("UserAnasayfa.aspx");
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            String u = TextBox1.Text;
            String p = TextBox2.Text;

            if (u == "admin")
            {

                            if (n.login(u, p))
                                {
                                     Session.Add("admin", u);
                                     Response.Redirect("Anasayfa.aspx");
                                 }
                            else
                                 {
                                     Label3.Visible = true;
                                     Label3.Text = "Basarisiz! Tekrar Deneyiniz! [Hata Kodu:TAN_41]";
                    TextBox1.Text = "";
                    TextBox2.Text = "";                                }
            }

            else
            {
                             if (n.login(u, p))
                             {
                             Session.Add("user", u);
                             Response.Redirect("UserAnasayfa.aspx");
                             }
                             else
                             {
                             Label3.Visible = true;
                             Label3.Text = "Basarisiz! Tekrar Deneyiniz! [Hata Kodu:TAN_41]";
                             }
          }



        }
    }
}