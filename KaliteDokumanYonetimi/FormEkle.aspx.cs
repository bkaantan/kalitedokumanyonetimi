﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.IO;

namespace KaliteDokumanYonetimi
{
    public partial class FormEkle : System.Web.UI.Page
    {
        Veritabani n = new Veritabani();
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);
            if (Session["user"] != null)
            {
                Page.MasterPageFile = "UserMasterPage.Master";
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                getCategory();
            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            string alan = "form";
            string no, kategori, path, ad;
            ad = TextBox1.Text;
            kategori = DropDownList2.SelectedValue;
            no = TextBox2.Text;
            path = "null";
            HttpPostedFile yuklenecekDosya = FileUpload1.PostedFile;
            if (yuklenecekDosya != null)
            {
                FileInfo dosyaBilgisi = new FileInfo(yuklenecekDosya.FileName);
                string klasor = "Dosyalar/Formlar";
                string dosyaAdi =  dosyaBilgisi.Name.Substring(0, dosyaBilgisi.Name.Length - dosyaBilgisi.Extension.Length);
                dosyaAdi += dosyaBilgisi.Extension;
                string yuklemeYeri = Server.MapPath("~/" + klasor + "/"+dosyaBilgisi.Name);
                FileUpload1.SaveAs(yuklemeYeri);
                path = klasor + "/" + dosyaAdi;
            }

            if (!path.Equals("null")) {
                
                n.addData(alan, kategori, no, path, ad);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('Dosya Yükleme Başarılı!') </script >");
                Response.Redirect("FormEkle.aspx");
            }
           

        }
        protected void getCategory() {
            try
            {
                MySqlDataReader dtReader;
                dtReader = n.listCategory();
                DropDownList2.DataSource = dtReader;
                DropDownList2.DataTextField = "ad";
                DropDownList2.DataValueField = "ad";
                
                DropDownList2.DataBind();

            }
            catch (Exception ex)
            {
                // Handle the error
            }

        }


    }
}