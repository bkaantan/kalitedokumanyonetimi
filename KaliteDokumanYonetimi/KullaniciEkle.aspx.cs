﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KaliteDokumanYonetimi
{
    public partial class KullaniciEkle : System.Web.UI.Page
    {
        Veritabani n = new Veritabani();

        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);
            if (Session["user"] != null)
            {
                Response.Redirect("/AdminLogin.aspx?islem=cik");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] != null)
            {
                Response.Redirect("/AdminLogin.aspx?islem=cik");
            }
        }

        [System.Web.Services.WebMethod]
        public static string SendParameters(string username, string name, string surname,string pass)
        {

            Veritabani n = new Veritabani();
            try
            {
                if (username.Equals("") || name.Equals("") || surname.Equals("") || pass.Equals(""))
                {
                    return string.Format("Bütün alanlar doldurulmalıdır! [Hata Kodu: TAN_43]", Environment.NewLine);
                }

                else if (n.getUser(username))
                {
                    return string.Format("Kullanıcı kayıtlı! [Hata Kodu: TAN_0]", Environment.NewLine);

                }
              
                else
                {
                    n.addUser(username, name, surname, pass);
                    return string.Format("Kullanıcı Adı: {0} Ad: {1} Soyad:{2} veritabanına eklendi! [İşlem Kodu: TAN_1]", username, name, surname, Environment.NewLine);

                }
            }
            catch {
                return string.Format("Bir hata meydana geldi! Lütfen daha sonra tekrar deneyiniz! [Hata Kodu:TAN_3]", Environment.NewLine);

            }


        }

        [System.Web.Services.WebMethod]
        public static string SendParameter(string name)
        {

            Veritabani n = new Veritabani();
            try
            {
                if (name.Equals("")) {

                    return string.Format("Adı alanı boş bırakılamaz! [Hata Kodu: TAN_43]", Environment.NewLine);

                }

                else if (n.getCat(name))
                {
                    return string.Format("Kategori kayıtlı! [Hata Kodu: TAN_0]", Environment.NewLine);

                }
                else
                {
                    n.addCat(name);
                    return string.Format("Kategori: {0} veritabanına eklendi! [İşlem Kodu: TAN_1]", name, Environment.NewLine);

                }
            }
            catch
            {
                return string.Format("Bir hata meydana geldi! Lütfen daha sonra tekrar deneyiniz! [Hata Kodu:TAN_3]", Environment.NewLine);

            }


        }

    }
}