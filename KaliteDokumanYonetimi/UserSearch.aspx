﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserSearch.aspx.cs" Inherits="KaliteDokumanYonetimi.UserSearch" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SKS Döküman Arama Sayfası</title>
      <link rel="stylesheet" href="content/bootstrap.min.css"/>
     <script src="scripts/bootstrap.min.js"></script>
     <script src="scripts/jquery-1.9.1.min.js"></script>
    <link rel="stylesheet" href="content/bootstrap.css"/>
     <link rel="stylesheet" href="content/css.css"/>
</head>
<body>
    
    <form id="form1" runat="server">
    <div>
     <div class="alert alert-info" style="margin-left:25px; margin-right:25px" role="alert"><h3><b>Çankırı Devlet Hastanesi SKS Döküman Arama Sayfası</b></h3></div>
         <div style="margin-left:25px;">
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox> <asp:Button ID="Button1" runat="server" Text="Ara" CssClass="btn btn-success" OnClick="Button1_Click" />
             <asp:Label ID="Label1" runat="server" BackColor="Red" Text=""></asp:Label>
       <asp:RadioButtonList ID="RadioButtonList1" runat="server">
            <asp:ListItem Value="form">Form</asp:ListItem>
            <asp:ListItem Value="prosedur">Prosedür</asp:ListItem>
            <asp:ListItem Value="dkd">Dış Kaynaklı Döküman</asp:ListItem>
            <asp:ListItem Value="talimat">Talimat</asp:ListItem>
            <asp:ListItem Value="gys">Görev Yetki Sorumluluk</asp:ListItem>
           <asp:ListItem Value="plan">Plan</asp:ListItem>
            <asp:ListItem Value="liste">Liste</asp:ListItem>

        </asp:RadioButtonList>
    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1"
    ControlToValidate="RadioButtonList1" Text="Alan Seçiniz!" BackColor="Red">
    </asp:RequiredFieldValidator>
         </div>

     <div style="margin-left:25px;">
                    <asp:GridView ID="GridView1" AllowCustomPaging="True" PageSize="15" runat="server" AutoGenerateColumns="False" DataKeyNames="id"  AllowPaging="True" BackColor="#CCCCCC" BorderColor="#999999" BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" ForeColor="Black">
        <Columns>
            <asp:TemplateField HeaderText="No">
                <EditItemTemplate>
                    <asp:TextBox ID="txtBoxNo" runat="server" Text='<%# Eval("no") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("no") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Ad">
                <EditItemTemplate>
                    <asp:TextBox ID="txtBoxAd" runat="server" Text='<%# Eval("ad") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("ad") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Birim">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownList1" runat="server">
                    </asp:DropDownList>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("kategori") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="İndir">
                <EditItemTemplate>
                    <asp:FileUpload ID="FileUpload1" runat="server" />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink CssClass="btn-sm btn-success" ID="HyperLink1" runat="server" NavigateUrl='<%# Eval("path") %>'>İndir</asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
                        <FooterStyle BackColor="#CCCCCC" />
                        <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                        <RowStyle BackColor="White" />
                        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                        <SortedAscendingHeaderStyle BackColor="#808080" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                        <SortedDescendingHeaderStyle BackColor="#383838" />
    </asp:GridView>
			</div>


        <script type="text/javascript">
		function isDelete()
		{
			return confirm("Do you want to delete this row ?");
		}
	</script>


    </div>
    </form>
    <nav class="navbar navbar-inverse navbar-fixed-bottom">
<nav class="navbar navbar-inverse navbar-fixed-bottom">

  <div class="navbar-header">
        <ul class="nav navbar-nav navbar-header">
        <li><a href="http://www.cankiridh.gov.tr/">Çankırı Devlet Hastanesi</a> </li>
      </ul>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">
        <li><a style="margin-right:15px;" href="mailto:burakkaan@outlook.com">Kaan TAN - 2016</a></li>
      </ul>
        
  </div><!-- /.navbar-collapse -->

</nav>
</nav>
</body>
</html>
