﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace KaliteDokumanYonetimi
{
    public partial class UserMasterPage : System.Web.UI.MasterPage
    {
        string a = "";
        Veritabani n = new Veritabani();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] != null)
            {
                a = Session["user"].ToString();
            }
            else if (Session["admin"] != null)
            {
                a = Session["admin"].ToString();
            }

            if (!a.Equals("")){ 
            MySqlDataReader dt1;
            dt1 = n.getUserName(a);
            dt1.Read();
            Label1.Text = dt1.GetString("isim").ToString() + " " + dt1.GetString("soyisim").ToString();
            }
            else
                Response.Redirect("/AdminLogin.aspx?islem=cik");
        }

    }
}