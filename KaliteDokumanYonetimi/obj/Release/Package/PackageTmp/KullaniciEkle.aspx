﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="KullaniciEkle.aspx.cs" Inherits="KaliteDokumanYonetimi.KullaniciEkle" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="alert alert-info" style="margin-left:25px; margin-right:25px" role="alert"><h3><b>Kullanıcı Ekle</b></h3></div>
    <div style="margin-left:25px;">
       
        <table border="0" cellpadding="0" cellspacing="0">
<tr>
    <td>
        <asp:Label ID="Label1" runat="server" Text=""><b>Kullanıcı Adı:</b></asp:Label>
    </td>
    <td>
        <asp:TextBox ID="txtUsername" runat="server" Text = "" />

    </td>
</tr>
<tr>
    <td>
        <asp:Label ID="Label2" runat="server" Text=""><b>Adı:</b></asp:Label>
    </td>
    <td>
        <asp:TextBox ID="txtName" runat="server" Text = ""/>

    </td>
</tr>
            <tr>
    <td>
        <asp:Label ID="Label3" runat="server" Text=""><b>Soyadı:</b></asp:Label>
    </td>
    <td>
        <asp:TextBox ID="txtSurname" runat="server" Text = ""/>

    </td>
</tr>
            <tr>
    <td>
        <asp:Label ID="Label4"  runat="server" Text=""><b>Şifre:</b></asp:Label>
    </td>
    <td>
          <asp:TextBox TextMode="Password" ID="txtPass" runat="server"/>

    </td>
</tr>


<tr>
    <td>

    </td><td></td>
    <td>
        <asp:Button ID="btnSubmit" Text="Kaydet" runat="server" CssClass="btn btn-success" />
        <div id='loadingmessage' style='display:none'>
       <img src='Images/ajax-loader.gif'/>
</div>
    </td>
</tr>
</table>
       

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/json2/20130526/json2.min.js"></script>
<script type="text/javascript">
$(function () {
    $("[id*=btnSubmit]").click(function () {
        var clientid = $("#client").val();
        $('#loadingmessage').show();
        var obj = {};
        obj.username = $.trim($("[id*=txtUsername]").val());
        obj.name = $.trim($("[id*=txtName]").val());
        obj.surname = $.trim($("[id*=txtSurname]").val());
        obj.pass = $.trim($("[id*=txtPass]").val());
        $.ajax({
            type: "POST",
            url: "KullaniciEkle.aspx/SendParameters",
            data: JSON.stringify(obj),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (r) {
                $('#loadingmessage').hide();
                alert(r.d);
                $("[id*=txtUsername]").val("");
                $("[id*=txtName]").val("");
                $("[id*=txtSurname]").val("");
                $("[id*=txtPass]").val("");
            }
        });
        return false;
    });
});
</script>


    </div>



</asp:Content>
