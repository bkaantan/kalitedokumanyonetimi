﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="KategoriEkle.aspx.cs" Inherits="KaliteDokumanYonetimi.KategoriEkle" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    
    <div class="alert alert-info" style="margin-left:25px; margin-right:25px" role="alert"><h3><b>Kategori Ekle</b></h3></div>
    <div style="margin-left:25px;">
       
        <table border="0" cellpadding="0" cellspacing="0">
<tr>
    <td>
        <asp:Label ID="Label1" runat="server" Text=""><b>Ad:</b></asp:Label>
        
    </td>
    <td>
        <asp:TextBox ID="txtName" runat="server" Text = "" />
        <asp:Button ID="btnSubmit" Text="Kaydet" runat="server" CssClass="btn btn-success" />
        <div id='loadingmessage' style='display:none'>
       <img src='Images/ajax-loader.gif'/>
</div>
        </td>
</tr>

<tr>
    <td>

    </td><td></td>
    <td>
        
    </td>
</tr>
</table>
       

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/json2/20130526/json2.min.js"></script>
<script type="text/javascript">
$(function () {
    $("[id*=btnSubmit]").click(function () {
        var clientid = $("#client").val();
        $('#loadingmessage').show();
        var val = $.trim($("[id*=txtName]").val());
        var obj = {};
        obj.name = val.toUpperCase();
        $.ajax({
            type: "POST",
            url: "KullaniciEkle.aspx/SendParameter",
            data: JSON.stringify(obj),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (r) {
                $('#loadingmessage').hide();
                alert(r.d);
                $("[id*=txtName]").val("");
               
            }
        });
        return false;
    });
});
</script>


    </div>










</asp:Content>
