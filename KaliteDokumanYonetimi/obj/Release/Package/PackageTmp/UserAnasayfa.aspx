﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserMasterPage.Master" AutoEventWireup="true" CodeBehind="UserAnasayfa.aspx.cs" Inherits="KaliteDokumanYonetimi.UserAnasayfa" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="orta">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="Images/a.jpg" alt="">
    </div>
      <div class="carousel-caption">
         <h3><b>Çankırı Devlet Hastanesi</b></h3>
    <p>SKS Döküman Yönetim Otomasyonu</p>
      </div>

  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
    </div>

<div class="container">
    <div class="page-header">
      
    </div>
    <div class="row grid-divider">
    <div class="col-sm-4">
      <div class="col-padding">
       <h3><b>Yükle</b></h3>
        <p>Döküman Yükle sekmesinden dosyalarınızı yükleyebilirsiniz.</p>
      </div>
    </div>
    <div class="col-sm-4">
      <div class="col-padding">
        <h3><b>Düzenle</b></h3>
        <p>Döküman Listele/Düzenle sekmesinden dosyalarınızı listeleyebilir, gerekli düzenlemeleri bu sayfalardan yapabilirsiniz.</p>
      </div>
    </div>
    <div class="col-sm-4">
      <div class="col-padding">
        <h3><b>Ara</b></h3>
        <p>Arama modülünü kullanarak dosyalarınıza kolaylıkla erişebilirsiniz.</p>
      </div>
    </div>
    </div>

</div>
</asp:Content>
