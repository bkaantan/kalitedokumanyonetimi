﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KaliteDokumanYonetimi
{
    public partial class SifreSifirla : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [System.Web.Services.WebMethod]
        public static string SendParameters(string email)
        {

            Veritabani n = new Veritabani();
            try
            {
                if (email.Equals(""))
                {
                    return string.Format("Email alanı doldurulmalıdır! [Hata Kodu: TAN_43]", Environment.NewLine);
                }

                else if (!n.getEmail(email))
                {
                    return string.Format("Kullanıcı kayıtlı değil! [Hata Kodu: TAN_0]", Environment.NewLine);

                }

                else
                {
                    tokenOlustur(email);
                    return string.Format("{0} e-posta adresinize şifre sıfırlama bağlantısı gönderildi! [İşlem Kodu: TAN_1]", email, Environment.NewLine);

                }
            }
            catch
            {
                return string.Format("Bir hata meydana geldi! Lütfen daha sonra tekrar deneyiniz! [Hata Kodu:TAN_3]", Environment.NewLine);

            }


        }


        public static void tokenOlustur(string email)
        {
            char[] karakter = "0123456789abcdefghijklmnoprstuvyz".ToCharArray(); //Şifrenin hangi harf ve sayılardan oluşacağını burada belirliyoruz
            string sonuc = "";
            Random rnd = new Random();
            for (int i = 0; i < 7; i++) 
            {
                sonuc += karakter[rnd.Next(0, karakter.Length - 1)].ToString();
            }
            Veritabani a = new Veritabani();
            a.addToken(email, sonuc);      
        }



    }
}