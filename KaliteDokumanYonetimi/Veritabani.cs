﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Data;

namespace KaliteDokumanYonetimi
{
    public class Veritabani
    {
        public static MySqlConnection baglan()
        {
            MySqlConnection baglanti;
            String baglantistringi;
            //baglantistringi = "Server=10.**.**.**;User Id=tek*****; Password=*****; Database=kalite; Pooling=false";
            baglantistringi = "Server=localhost;User Id=root; Password=root; Database=kalite; Pooling=false";
            baglanti = new MySqlConnection(baglantistringi);
            baglanti.Open();
            return baglanti;
        }
        public bool login(string username, string password)
        {

            MySqlConnection bag = baglan();
            String sorgu;
            sorgu = "SELECT * FROM kullanici Where kullanici_adi='" + username + "' and sifre='" + password + "'";
            MySqlDataReader dtReader;
            MySqlCommand sqlkomut;
            sqlkomut = new MySqlCommand(sorgu, bag);
            dtReader = sqlkomut.ExecuteReader();

            return dtReader.Read();
        }

        public bool getUser(string username)
        {

            MySqlConnection bag = baglan();
            String sorgu;
            sorgu = "SELECT * FROM kullanici Where kullanici_adi='" + username + "'";
            MySqlDataReader dtReader;
            MySqlCommand sqlkomut;
            sqlkomut = new MySqlCommand(sorgu, bag);
            dtReader = sqlkomut.ExecuteReader();

            return dtReader.Read();

        }

        public bool getEmail(string email)
        {

            MySqlConnection bag = baglan();
            String sorgu;
            sorgu = "SELECT * FROM kullanici Where email='" + email + "'";
            MySqlDataReader dtReader;
            MySqlCommand sqlkomut;
            sqlkomut = new MySqlCommand(sorgu, bag);
            dtReader = sqlkomut.ExecuteReader();

            return dtReader.Read();

        }

        public MySqlDataReader getUserName(string username)
        {

            MySqlConnection bag = baglan();
            MySqlCommand sqlkomut;
            MySqlDataReader dtReader;
            String sorgu;
            sorgu = "SELECT * from kullanici where kullanici_adi='" + username + "'";
            sqlkomut = new MySqlCommand(sorgu, bag);

            dtReader = sqlkomut.ExecuteReader();

            return dtReader;
        }
        public MySqlDataReader listCategory()
        {

            MySqlConnection bag = baglan();
            MySqlCommand sqlkomut;
            MySqlDataReader dtReader;
            String sorgu;
            sorgu = "SELECT ad from kategori";
            sqlkomut = new MySqlCommand(sorgu, bag);

            dtReader = sqlkomut.ExecuteReader();

            return dtReader;
        }
        public void addData(string alan, string kategori, string no, string path,string ad)
        {

            MySqlConnection bag = baglan();
            MySqlCommand sqlkomut;

            String sorgu;
            sorgu = "INSERT INTO "+alan+" (kategori,no,path,ad) VALUES('" 
            + kategori + "'," + "'" + no + "'," + "'" 
            + path + "',"+"'"+ad+"')";
            sqlkomut = new MySqlCommand(sorgu, bag);
            sqlkomut.ExecuteNonQuery();
            bag.Close();
        }
        public DataTable listForm(string alan)
        {

            MySqlConnection bag = baglan();
            MySqlCommand sqlkomut;
            String sorgu;
            sorgu = "SELECT id,no,ad,kategori,path from "+alan;
            sqlkomut = new MySqlCommand(sorgu, bag);

            DataTable dataTable = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(sqlkomut);

            da.Fill(dataTable);


            return dataTable;
        }
        public void updateData(string alan, string kategori, string no, string id, string ad)
        {

            MySqlConnection bag = baglan();
            MySqlCommand sqlkomut;

            String sorgu;
            sorgu = "UPDATE " + alan + " SET kategori='" + kategori + "',no="
                + "'" + no + "',ad=" + "'" + ad + "' WHERE id='"+id+"'";
            sqlkomut = new MySqlCommand(sorgu, bag);
            sqlkomut.ExecuteNonQuery();
            bag.Close();
        }
        public void updateDataWithFile(string alan, string kategori, string no, 
            string id, string ad,string path)
        {

            MySqlConnection bag = baglan();
            MySqlCommand sqlkomut;

            String sorgu;
            sorgu = "UPDATE " + alan + " SET kategori='" + kategori + "',no=" + "'" + no + "',ad="
                + "'" + ad + "',path='"+path+"' WHERE id='" + id + "'";
            sqlkomut = new MySqlCommand(sorgu, bag);
            sqlkomut.ExecuteNonQuery();
            bag.Close();
        }
        public DataTable listCat()
        {

            MySqlConnection bag = baglan();
            MySqlCommand sqlkomut;
            String sorgu;
            sorgu = "SELECT ad from kategori";
            sqlkomut = new MySqlCommand(sorgu, bag);

            DataTable dataTable = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(sqlkomut);

            da.Fill(dataTable);


            return dataTable;
        }
        public void deleteFileFromDB(string alan, string id)
        {

            MySqlConnection bag = baglan();
            MySqlCommand sqlkomut;

            String sorgu;
            sorgu = "DELETE FROM " + alan + " WHERE id='" + id + "'";
            sqlkomut = new MySqlCommand(sorgu, bag);
            sqlkomut.ExecuteNonQuery();
            bag.Close();
        }
        public MySqlDataReader getFilePath(string alan,string id)
        {

            MySqlConnection bag = baglan();
            MySqlCommand sqlkomut;
            MySqlDataReader dtReader;
            String sorgu;
            sorgu = "SELECT path FROM " + alan + " WHERE id='" + id + "'";
            sqlkomut = new MySqlCommand(sorgu, bag);

            dtReader = sqlkomut.ExecuteReader();

            return dtReader;
        }

        public void addUser(string username, string name, string surname, string pass)
        {

            MySqlConnection bag = baglan();
            MySqlCommand sqlkomut;

            String sorgu;
            sorgu = "INSERT INTO kullanici (kullanici_adi,isim,soyisim,sifre) VALUES('" + username + "'," + "'" + name + "'," + "'" + surname + "'," + "'" + pass+ "')";
            sqlkomut = new MySqlCommand(sorgu, bag);
            sqlkomut.ExecuteNonQuery();
            bag.Close();
        }

        public void addCat(string name)
        {

            MySqlConnection bag = baglan();
            MySqlCommand sqlkomut;

            String sorgu;
            sorgu = "INSERT INTO kategori (ad) VALUES('" + name + "')";
            sqlkomut = new MySqlCommand(sorgu, bag);
            sqlkomut.ExecuteNonQuery();
            bag.Close();
        }
        public void addToken(string email,string token)
        {

            MySqlConnection bag = baglan();
            MySqlCommand sqlkomut;

            String sorgu;
            sorgu = "INSERT INTO token (email,token,seen) VALUES('" + email + "','"+token+"','"+0+"')";
            sqlkomut = new MySqlCommand(sorgu, bag);
            sqlkomut.ExecuteNonQuery();
            bag.Close();
        }


        public bool getCat(string name)
        {

            MySqlConnection bag = baglan();
            String sorgu;
            sorgu = "SELECT * FROM kategori Where ad='" + name + "'";
            MySqlDataReader dtReader;
            MySqlCommand sqlkomut;
            sqlkomut = new MySqlCommand(sorgu, bag);
            dtReader = sqlkomut.ExecuteReader();

            return dtReader.Read();

        }

        public DataTable searchDoc(string alan,string str)
        {

            MySqlConnection bag = baglan();
            MySqlCommand sqlkomut;
            String sorgu;
            sorgu = "SELECT id,no,ad,kategori,path from " + alan+ " WHERE ad LIKE '%"+str+"%'";
            sqlkomut = new MySqlCommand(sorgu, bag);

            DataTable dataTable = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(sqlkomut);

            da.Fill(dataTable);


            return dataTable;
        }


    }


}