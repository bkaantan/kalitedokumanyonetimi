﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KaliteDokumanYonetimi
{
    public partial class UserSearch : System.Web.UI.Page
    {
        Veritabani n = new Veritabani();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                

            }

        }

        protected void bindData()
        {
            DataTable dataTable = new DataTable();
            dataTable = n.listForm("form");
            GridView1.DataSource = dataTable;
            GridView1.DataBind();
        }


        protected string getCurrentFilePath(string alan, string id)
        {
            MySqlDataReader dt1;
            dt1 = n.getFilePath(alan, id);
            dt1.Read();
            string path = dt1.GetString("path").ToString();

            return path;

        }


        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dataTable = new DataTable();
                string str = RadioButtonList1.SelectedValue;
                string txt = TextBox1.Text;
                dataTable = n.searchDoc(str, txt);
                if (dataTable.Rows.Count > 0)
                {
                    GridView1.DataSource = dataTable;
                    GridView1.DataBind();
                    Label1.Text = "";
                }
                else
                {
                    Label1.Text = "Dosya Bulunamadı! [İşlem Kodu:TAN_54]";
                }
            }
            catch {

                Label1.Text = "Dosya Bulunamadı! [İşlem Kodu:TAN_3]";
            }



        }
    }
}