﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KaliteDokumanYonetimi
{
    public partial class ListProsedur : System.Web.UI.Page
    {

        Veritabani n = new Veritabani();

        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);
            if (Session["user"] != null)
            {
                Page.MasterPageFile = "UserMasterPage.Master";
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindData();

            }

        }


        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            bindData();
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            string path = "null";

            string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
            TextBox no = (TextBox)GridView1.Rows[e.RowIndex].FindControl("txtBoxNo");
            TextBox ad = (TextBox)GridView1.Rows[e.RowIndex].FindControl("txtBoxAd");
            DropDownList dd = (DropDownList)GridView1.Rows[e.RowIndex].FindControl("DropdownList1");
            deleteFileFromFolder(getCurrentFilePath("prosedur", id));

            FileUpload FileUpload1 = (FileUpload)GridView1.Rows[e.RowIndex].FindControl("FileUpload1");
            HttpPostedFile yuklenecekDosya = FileUpload1.PostedFile;

            if (yuklenecekDosya != null && FileUpload1.HasFile)
            {
                FileInfo dosyaBilgisi = new FileInfo(yuklenecekDosya.FileName);
                string klasor = "Dosyalar/Prosedurler";
                string dosyaAdi = dosyaBilgisi.Name.Substring(0, dosyaBilgisi.Name.Length - dosyaBilgisi.Extension.Length);
                dosyaAdi += dosyaBilgisi.Extension;
                string yuklemeYeri = Server.MapPath("~/" + klasor + "/" + dosyaBilgisi.Name);
                FileUpload1.SaveAs(yuklemeYeri);
                path = klasor + "/" + dosyaAdi;
            }

            if (!path.Equals("null"))
            {
                n.updateDataWithFile("prosedur", dd.SelectedValue, no.Text, id, ad.Text, path);
            }
            else
            {
                n.updateData("prosedur", dd.SelectedValue, no.Text, id, ad.Text);
            }

            GridView1.EditIndex = -1;
            bindData();

        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            bindData();
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

            string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
            deleteFileFromFolder(getCurrentFilePath("prosedur", id));
            n.deleteFileFromDB("prosedur", id);
            GridView1.EditIndex = -1;
            bindData();
        }

        protected void bindData()
        {
            DataTable dataTable = new DataTable();
            dataTable = n.listForm("prosedur");
            if (dataTable.Rows.Count > -1)
            {
                GridView1.DataSource = dataTable;
                GridView1.DataBind();
                Label1.Text = "";
            }
            else
            {
                Label1.Text = "Dosya Bulunamadı! [İşlem Kodu:TAN_54]";
            }
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if ((e.Row.RowState & DataControlRowState.Edit) > 0)
                {
                    DropDownList ddList = (DropDownList)e.Row.FindControl("DropdownList1");
                    //bind dropdown-list
                    DataTable dt = new DataTable();
                    dt = n.listCat();
                    ddList.DataSource = dt;
                    ddList.DataTextField = "ad";
                    ddList.DataValueField = "ad";
                    ddList.DataBind();

                    DataRowView dr = e.Row.DataItem as DataRowView;
                    //ddList.SelectedItem.Text = dr["category_name"].ToString();
                    ddList.SelectedValue = dr["ad"].ToString();
                }
            }
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            bindData();
        }

        protected string getCurrentFilePath(string alan, string id)
        {
            MySqlDataReader dt1;
            dt1 = n.getFilePath(alan, id);
            dt1.Read();
            string path = dt1.GetString("path").ToString();

            return path;

        }

        protected void deleteFileFromFolder(string path)
        {

            FileInfo file = new FileInfo(path);
            if (file.Exists)//check file exsit or not
                File.Delete(path);
        }




    }
}