﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KaliteDokumanYonetimi
{
    public partial class KategoriEkle : System.Web.UI.Page
    {
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);
            if (Session["user"] != null)
            {
                Response.Redirect("/AdminLogin.aspx?islem=cik");
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] != null)
            {
                Response.Redirect("/AdminLogin.aspx?islem=cik");
            }
        }
    }
}