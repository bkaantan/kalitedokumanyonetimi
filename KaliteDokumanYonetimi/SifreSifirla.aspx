﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SifreSifirla.aspx.cs" Inherits="KaliteDokumanYonetimi.SifreSifirla" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <title>Şifremi Unuttum?</title>
      <link rel="stylesheet" href="content/bootstrap.min.css"/>
     <script src="scripts/bootstrap.min.js"></script>
     <script src="scripts/jquery-1.9.1.min.js"></script>
    <link rel="stylesheet" href="content/bootstrap.css"/>
     <link rel="stylesheet" href="content/css.css"/>
</head>
<body>
    <form id="form1" runat="server">

      <div>
     <div class="alert alert-info" style="margin-left:25px; margin-right:25px" role="alert"><h3><b>Şifremi Unuttum?</b></h3></div>
    <div style="margin-left:25px;">
    <tr>
    <td>
        <asp:Label ID="Label1" runat="server" Text=""><b>E-mail:</b></asp:Label>
    </td>
    <td>
        <asp:TextBox ID="txtEmail" runat="server" Text = "" />
         <asp:Button ID="btnSubmit" Text="Kaydet" runat="server" CssClass="btn btn-success" />
        <div id='loadingmessage' style='display:none'>
       <img src='Images/ajax-loader.gif'/>
    </td>
</tr>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/json2/20130526/json2.min.js"></script>
<script type="text/javascript">
$(function () {
    $("[id*=btnSubmit]").click(function () {
        var clientid = $("#client").val();
        $('#loadingmessage').show();
        var obj = {};
        obj.email = $.trim($("[id*=txtEmail]").val());
        $.ajax({
            type: "POST",
            url: "SifreSifirla.aspx/SendParameters",
            data: JSON.stringify(obj),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (r) {
                $('#loadingmessage').hide();
                alert(r.d);
                $("[id*=txtEmail]").val("");

            }
        });
        return false;
    });
});
</script>           
    </form>
    </div>
<nav class="navbar navbar-inverse navbar-fixed-bottom">
<nav class="navbar navbar-inverse navbar-fixed-bottom">

  <div class="navbar-header">
        <ul class="nav navbar-nav navbar-header">
        <li><a href="http://www.cankiridh.gov.tr/">Çankırı Devlet Hastanesi</a> </li>
      </ul>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">
        <li><a style="margin-right:15px;" href="mailto:burakkaan@outlook.com">Kaan TAN - 2016</a></li>
      </ul>
        
  </div><!-- /.navbar-collapse -->

</nav>
</nav>
</body>
</html>
