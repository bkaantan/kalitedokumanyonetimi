﻿<%@ Page Title="" Language="C#" EnableViewState="true" EnableEventValidation="false" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="DKDEkle.aspx.cs" Inherits="KaliteDokumanYonetimi.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <div class="alert alert-info" style="margin-left:25px; margin-right:25px" role="alert"><h3><b>Dış Kaynaklı Döküman Ekle</b></h3></div>
    <div style="margin-left:25px;">
    <form id="form1">
        
  <table  style="width:45%">
  <tr>
    <td><asp:Label ID="Label1" runat="server" Text="Label" CssClass="h4">Ad:</asp:Label></td>
    <td><asp:TextBox ID="TextBox1" runat="server"></asp:TextBox></td> 
      <asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server"
        ControlToValidate="TextBox1"
        ErrorMessage="Ad alanı doldurulmalıdır! "
        ForeColor="Red">
      </asp:RequiredFieldValidator>
  </tr>
  <tr>
    <td><asp:Label ID="Label5" runat="server" Text="Label" CssClass="h4">Kategori:</asp:Label></td>
    <td><asp:DropDownList ID="DropDownList2" runat="server"> </asp:DropDownList></td> 
      <asp:RequiredFieldValidator ID="rfv1" runat="server" 
          ControlToValidate="DropDownList2" 
          InitialValue="0" 
          ErrorMessage="Lütfen Kategori Seçiniz! " 
          ForeColor="Red"/>
  </tr>
  <tr>
    <td><asp:Label ID="Label3" runat="server" Text="Label" CssClass="h4">Döküman No:</asp:Label></td>
    <td><asp:TextBox ID="TextBox2" runat="server"></asp:TextBox></td> 
    <asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server"
             ControlToValidate="TextBox2"
             ErrorMessage="Döküman No alanı doldurulmalıdır! "
             ForeColor="Red">
    </asp:RequiredFieldValidator>
  </tr>
  <tr>
    <td><asp:Label ID="Label4" runat="server" Text="Label" CssClass="h4">Dosya:</asp:Label></td>
    <td><asp:FileUpload ID="FileUpload1" runat="server" /> </td> 
      <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"
          ControlToValidate="FileUpload1" 
          ErrorMessage="Dosya Seçiniz! " 
          ForeColor="Red">
        </asp:RequiredFieldValidator>  

  </tr>
  <tr>
    <td></td>
    <td><asp:Button ID="Button2" runat="server" Text="Dosya Temizle" CssClass="btn-sm btn-danger"/></td>
  </tr>
      <tr>
          <td></td>
          <td></td>
         <td><asp:Button ID="Button1" runat="server" Text="DKD Yükle" CssClass="btn-lg btn-success" OnClick="Button1_Click"/></td> 
      </tr>

</table>
    
    </form>

    </div>
    
    <script type="text/javascript">
    $(function () {
    $("[id*=Button2]").click(function () {
        //Reference the FileUpload and get its Id and Name.
        var fileUpload = $("[id*=FileUpload1]");
        var id = fileUpload.attr("id");
        var name = fileUpload.attr("name");
 
        //Create a new FileUpload element.
        var newFileUpload = $("<input type = 'file' />");
 
        //Append it next to the original FileUpload.
        fileUpload.after(newFileUpload);
 
        //Remove the original FileUpload.
        fileUpload.remove();
 
        //Set the Id and Name to the new FileUpload.
        newFileUpload.attr("id", id);
        newFileUpload.attr("name", name);
        return false;
    });
    });


</script>
  



</asp:Content>
